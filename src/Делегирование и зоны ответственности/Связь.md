### Связь

Создайте поток информации и влияния между двумя командами.

Команда выбирает одного из своих членов как представителя ее интересов в обсуждении управленческих решений, принимаемых другой командой.
